﻿using System;

namespace PrintSquare
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input a number:");
            int size = Convert.ToInt32(Console.ReadLine());          
            PrintStars(size);
        }

        static void PrintStars(int size)
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    Console.Write("*");
                }
                Console.WriteLine("");
            }
        }
    }
}